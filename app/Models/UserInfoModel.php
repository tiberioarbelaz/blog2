<?php

namespace App\Models;

use CodeIgniter\Model;


class UserInfoModel extends Model
{
    protected $table      = 'info_users';
    protected $primaryKey = 'id_user';

    protected $returnType     = 'object';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['name', 'lastname', 'id_country'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
}

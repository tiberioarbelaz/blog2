<?= $this->extend('Front/layout/main') ?>

<?= $this->section('title') ?>
Registro
<?= $this->endSection() ?>
<?= $this->section('css') ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
<?= $this->endSection() ?>
<?= $this->section('content') ?>
<section class="section">
    <div class="container">
        <h1 class="title">Registrate ahora!</h1>
        <h2 class="subtitle">
            A simple container to divide your page into <strong>sections</strong>, like the one you're currently reading.
        </h2>
        <?php session() ?>
        <form action="<?= base_url('auth/store'); ?>" method="POST">
            <div class="field">
                <label class="label">Nombre</label>
                <div class="control">
                    <input class="input" name="name" value="<?= old('name') ?>" type="text" placeholder="Introduce tu nombre">
                </div>
                <p class="is-danger help"><?= session('errors.name') ?></p>
            </div>

            <div class="field">
                <label class="label">Apellido</label>
                <div class="control">
                    <input class=" input is-success" name="lastname" value="<?= old('lastname') ?>" type="text" placeholder="Introduce tu apellido">
                </div>
                <p class="is-danger help"><?= session('errors.lastname') ?></p>
            </div>

            <div class="field">
                <label class="label">Email</label>
                <div class="control has-icons-left has-icons-right">
                    <input class="input" name="email" value="<?= old('email') ?>" type="email" placeholder="Introduce tu correo">
                    <span class="icon is-small is-left">
                        <i class="fas fa-envelope"></i>
                    </span>

                </div>
                <p class="is-danger help"><?= session('errors.email') ?></p>
            </div>

            <div class="field">
                <label class="label">Elije tu pais</label>
                <div class="control">
                    <div class="select">
                        <select name="id_country">
                            <option disabled selected>Seleccione pais</option>
                            <?php foreach ($countries as $v) : ?>

                                <option value="<?= $v->id_country ?>" <?php if ($v->id_country == old('id_country')) : ?> selected <?php endif ?>><?= $v->name ?></option>

                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <p class="is-danger help"><?= session('errors.id_country') ?></p>
            </div>
            <div class="field">
                <label class="label">Contraseña</label>
                <div class="control">
                    <input class=" input is-success" name="password" type="password" placeholder="Introduce tu contraseña">
                </div>
                <p class="is-danger help"><?= session('errors.password') ?></p>
            </div>

            <div class="field">
                <label class="label">Repite tu contraseña</label>
                <div class="control">
                    <input class=" input is-success" name="c-password" type="password" placeholder="Introduce tu contraseña">
                </div>

            </div>
            <div class="field is-grouped">
                <div class="control">
                    <button class="button is-info">Registrarme</button>
                </div>
            </div>
        </form>
    </div>

</section>
<?= $this->endSection() ?>
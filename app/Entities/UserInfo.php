<?php

namespace APP\Entities;

use CodeIgniter\Entity;

class UserInfo extends Entity
{
    protected $dates = ['created_at', 'updated_at'];
}
